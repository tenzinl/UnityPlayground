﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngineInternal;

// ArcDrawerAttempt2 is a script that should be attached to an empty game object in
// the scene. It draws the arc formed by the line drawn by three points using Unity's
// LineRenderer component.
public class ArcDrawerAttempt2 : MonoBehaviour
{
    LineRenderer line;
    Vector3[] jointPositions = new Vector3[3];
    private const float PI = Mathf.PI;
    public const float ARC_DRAW_RADIUS = 4;
    ObjectClicker clicker = new ObjectClicker();
    // Start is called before the first frame update
    void Start() {
        line = GetComponent<LineRenderer>();
        setLineWidth(line, 0.15f);
        updateJointPositions(GameObject.Find("Joints"));
        drawArc();
    }

    // Update is called every frame
    private void Update() {
        clicker.checkClick();
        updateJointPositions(GameObject.Find("Joints"));
        drawArc();
    }

    // Updates jointPositions array with new information given a parent game object
    private void updateJointPositions(GameObject jointParent) {
        for (int i = 0; i < 3; i++) {
            jointPositions[i] = jointParent.transform.GetChild(i).position;
        }
    }

    // Set a line renderer components start and end width to the given value
    // Basically setting its overall width.
    void setLineWidth(LineRenderer l, float width) {
        l.startWidth = width;
        l.endWidth = width;
    }

    // Return's a position vector's theta in the spherical coordinate system
    // Theta starts at x and goes towards y of global axes, all theta values
    // returned will be 0 <= theta <= 2 * PI. 
    float getTheta(Vector3 vector) {
        // Don't worry about divide by 0 etc. as IEEE 754 provisions for floats to represent NaN and INF
        if (Mathf.Abs(vector.x) <= 0.001 && Mathf.Abs(vector.y) <= 0.001) {
            return PI / 2;
        }
        float theta = Mathf.Atan(vector.y / vector.x);
        if (theta > 0 && vector.y < 0) {
            theta += Mathf.PI;
        }
        if (theta < 0) {
            theta += Mathf.PI;
            if (vector.y < 0) {
                theta += Mathf.PI;
            }
        }
        return theta;
    }

    // Return's a position vector's phi in the spherical coordinate system
    float getPhi(Vector3 vector) {
        float phi = Mathf.Acos(vector.z / vector.magnitude);
        return phi;
    }

    // Returns a position vectors equivalent coordinates if the coordinate
    // axes were rotated around the z axis by theta radians (x towards y)
    Vector3 rotateAroundZ(Vector3 v, float theta) {
        Vector3 ans;
        ans.x = v.x * Mathf.Cos(theta) + v.y * Mathf.Sin(theta);
        ans.y = -v.x * Mathf.Sin(theta) + v.y * Mathf.Cos(theta);
        ans.z = v.z;
        return ans;
    }

    // Returns a position vectors equivalent coordinates if the coordinate
    // axes were rotated around the x axis by phi radians (z towards y)
    Vector3 rotateAroundX(Vector3 v, float phi) {
        Vector3 ans;
        ans.x = v.x;
        ans.y = v.y * Mathf.Cos(phi) - v.z * Mathf.Sin(phi);
        ans.z = v.y * Mathf.Sin(phi) + v.z * Mathf.Cos(phi);
        return ans;
    }

    // Returns a position vector's coordinates if the coordinate
    // axes were rotated around the z-axis by theta, and then
    // around the x axis by phi.
    Vector3 rotateAxes(Vector3 v, float theta, float phi) {
        v = rotateAroundZ(v, theta);
        v = rotateAroundX(v, phi);
        return v;
    }

    // Returns a coordinates global coordinates after having
    // been rotated by theta and phi.
    Vector3 unrotateAxes(Vector3 v, float theta, float phi) {
        v = rotateAroundX(v, -phi);
        v = rotateAroundZ(v, -theta);
        return v;
    }
    // AGAIN. I DID ROTATE AROUNDX TWICE WTHFHS

    // A debugging function, print's a Vector3's coordinates and
    // name in a nice format.
    void printVector(Vector3 v, string vectorName = "Vector") {
        print(vectorName + "'s coordinates: (" + v.x + ", " + v.y + ", " + v.z + ")");
    }

    // Converts a spherical coordinate to its corresponding cartesian coordinate in
    // the same reference frame.
    Vector3 sphericalPositionToCartesian(float radius, float theta, float phi) {
        Vector3 ans = new Vector3(0, 0, 0);
        ans.z = radius * Mathf.Cos(phi);
        float XYMagnitude = radius * Mathf.Sin(phi);
        ans.x = XYMagnitude * Mathf.Cos(theta);
        ans.y = XYMagnitude * Mathf.Sin(theta);
        return ans;
    }

    // Draws the arc formed by the line drawn by three points.
    void drawArc() {
        Vector3[] relativePositions = new Vector3[3];
        for (int i = 0; i < 3; i++) {
            relativePositions[i] = jointPositions[i] - jointPositions[1];
        }

        float[] thetas = new float[3];
        float[] phis = new float[3];
        for (int i = 0; i < 3; i++) {
            thetas[i] = getTheta(relativePositions[i]);
            phis[i] = getPhi(relativePositions[i]);
        }

        float rotateTheta = thetas[0] - PI / 2;
        float rotatePhi = phis[0];

        Vector3[] rotatedPositions = new Vector3[3];

        rotatedPositions[0] = new Vector3(0, 0, relativePositions[0].magnitude);

        rotatedPositions[2] = rotateAxes(relativePositions[2], rotateTheta, rotatePhi);
        float rotatedTheta = getTheta(rotatedPositions[2]);
        float rotatedPhi = getPhi(rotatedPositions[2]);

        float ANGLE_DEGREES = Vector3.Angle(relativePositions[0], relativePositions[2]);
        int numberOfPoints = (int)(ANGLE_DEGREES / 5);

        float deltaPhi = rotatedPhi / numberOfPoints;
        Vector3[] rotatedLinePositions = new Vector3[40];
        Vector3[] linePositions = new Vector3[40];
        linePositions[0] = unrotateAxes(rotatedLinePositions[0], rotateTheta, rotatePhi);
        for (int i = 0; i <= numberOfPoints; i++) {
            float curPhi = i * deltaPhi;
            rotatedLinePositions[i] = sphericalPositionToCartesian(ARC_DRAW_RADIUS, rotatedTheta, curPhi);
            linePositions[i] = unrotateAxes(rotatedLinePositions[i], rotateTheta, rotatePhi) + jointPositions[1];
        }

        line.positionCount = numberOfPoints + 1;
        line.SetPositions(linePositions);

        // YESSS IT WORKSSS
    }
}

// The equations may be fundamentally wrong since it's a left handed coordinate system. Currently it looks like rotateTheta works fine, I'll need
// to investigate rotatePhi. I think there might be a wrong sign somewhere since it's left handed. Nope you dumbbutt no difference. That's why we were able
// to choose right hand system, since it's arbitrary as long as you're internally consistent. It's cuz you USED THE SAME ROTATION TWICE jeez.

// IM SO BIG DUMB. I TYPED ROTATE AROUND Z TWICE goshdarnit. YESSS IT WOOORRKKS!!!