﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JointMover : MonoBehaviour
{
    // Start is called before the first frame update
    private Transform myTransform;
    private Vector3 direction;
    void Start()
    {
        myTransform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        direction.x = Input.GetAxisRaw("Horizontal");
        direction.z = Input.GetAxisRaw("Vertical");

        myTransform.position += direction * 60 * Time.deltaTime;
    }
}
