﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectClicker {
    [SerializeField] private Material defaultMaterial;
    [SerializeField] private Material highlightedMaterial;

    Ray ray;
    RaycastHit hit;
    bool clicked;
    GameObject currentlySelected = null;

    public void checkClick() {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Input.GetMouseButtonDown(0)) {
            //print("hi");
            if (currentlySelected != null) {
                currentlySelected.GetComponent<MeshRenderer>().material = defaultMaterial;
            }
            if (Physics.Raycast(ray, out hit, 100.0f)) {
                if (hit.transform != null) {
                    printName(hit.transform.gameObject);
                    currentlySelected = hit.transform.gameObject;
                    currentlySelected.GetComponent<MeshRenderer>().material = highlightedMaterial;
                }
            }
        }
    }
    private void printName(GameObject go) {
        Debug.Log(go.name);
    }
}
