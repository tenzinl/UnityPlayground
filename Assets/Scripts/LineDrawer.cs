﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDrawer : MonoBehaviour
{
    private Vector3[] positions = new Vector3[3];
    private GameObject jointParent;
    private Transform[] joints = new Transform[3];
    private LineRenderer line;
    // Start is called before the first frame update
    void Start()
    {
        line = GetComponent<LineRenderer>();
        jointParent = GameObject.Find("Joints");
        int jointCount = jointParent.transform.childCount;
        for (int i = 0; i < 3; i++) {
            joints[i] = jointParent.transform.GetChild(i); // I'm assuming this will pass by reference
            positions[i] = joints[i].position;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        line.positionCount = 3;
        for (int i = 0; i < 3; i++) { // I'm a dumbass I had 2 instead of 3, so it was only adding indexes 1 and 2.
            positions[i] = joints[i].position;
        }
        line.SetPositions(positions);
    }
}
