﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager
{
    public static AnimationData[] LoadData(string path)
    {
        List<Dictionary<string, object>> data = CSVReader.Read(path);
        return ParseAnimationData(data);
    }

    public static AnimationData[] LoadDataString(string csv)
    {
        List<Dictionary<string, object>> data = CSVReader.ReadString(csv);
        return ParseAnimationData(data);
    }

    private static AnimationData[] ParseAnimationData(List<Dictionary<string, object>> data)
    {
        AnimationData[] animationData = new AnimationData[data.Count];

        for (int i = 0; i < animationData.Length; i++)
        {
            AnimationData frame = new AnimationData();

            // Data: Pelvis XYZ
            frame.hip_x = (float)data[i]["Pelvis_x"];
            frame.hip_y = -(float)data[i]["Pelvis_y"];
            frame.hip_z = (float)data[i]["Pelvis_z"];

            // Data: Torso XYZ
            frame.spine_x = (float)data[i]["Torso_x"];
            frame.spine_y = -(float)data[i]["Torso_y"];
            frame.spine_z = (float)data[i]["Torso_z"];

            // Data: LR Hip XYZ
            frame.l_upleg_x = (float)data[i]["L_Hip_x"];
            frame.l_upleg_y = -(float)data[i]["L_Hip_y"];
            frame.l_upleg_z = (float)data[i]["L_Hip_z"];

            frame.r_upleg_x = (float)data[i]["R_Hip_x"];
            frame.r_upleg_y = -(float)data[i]["R_Hip_y"];
            frame.r_upleg_z = (float)data[i]["R_Hip_z"];

            // Data: LR Knee XYZ
            frame.l_leg_x = (float)data[i]["L_Knee_x"];
            frame.l_leg_y = -(float)data[i]["L_Knee_y"];
            frame.l_leg_z = (float)data[i]["L_Knee_z"];

            frame.r_leg_x = (float)data[i]["R_Knee_x"];
            frame.r_leg_y = -(float)data[i]["R_Knee_y"];
            frame.r_leg_z = (float)data[i]["R_Knee_z"];

            // Data: LR Ankle XYZ
            frame.l_foot_x = (float)data[i]["L_Ankle_x"];
            frame.l_foot_y = -(float)data[i]["L_Ankle_y"];
            frame.l_foot_z = (float)data[i]["L_Ankle_z"];

            frame.r_foot_x = (float)data[i]["R_Ankle_x"];
            frame.r_foot_y = -(float)data[i]["R_Ankle_y"];
            frame.r_foot_z = (float)data[i]["R_Ankle_z"];

            // Data: Neck XYZ
            frame.neck_x = (float)data[i]["Neck_x"];
            frame.neck_y = -(float)data[i]["Neck_y"];
            frame.neck_z = (float)data[i]["Neck_z"];

            // Data: Head XYZ
            frame.head_x = (float)data[i]["Head_x"];
            frame.head_y = -(float)data[i]["Head_y"];
            frame.head_z = (float)data[i]["Head_z"];

            // Data: Nose XYZ
            frame.nose_x = (float)data[i]["Nose_x"];
            frame.nose_y = -(float)data[i]["Nose_y"];
            frame.nose_z = (float)data[i]["Nose_z"];

            // Data: LR Shoulder XYZ (Could also be arm)
            frame.l_arm_x = (float)data[i]["L_Shoulder_x"];
            frame.l_arm_y = -(float)data[i]["L_Shoulder_y"];
            frame.l_arm_z = (float)data[i]["L_Shoulder_z"];

            frame.r_arm_x = (float)data[i]["R_Shoulder_x"];
            frame.r_arm_y = -(float)data[i]["R_Shoulder_y"];
            frame.r_arm_z = (float)data[i]["R_Shoulder_z"];

            // Data: LR Elbow XYZ
            frame.l_forearm_x = (float)data[i]["L_Elbow_x"];
            frame.l_forearm_y = -(float)data[i]["L_Elbow_y"];
            frame.l_forearm_z = (float)data[i]["L_Elbow_z"];

            frame.r_forearm_x = (float)data[i]["R_Elbow_x"];
            frame.r_forearm_y = -(float)data[i]["R_Elbow_y"];
            frame.r_forearm_z = (float)data[i]["R_Elbow_z"];

            // Data: LR Wrist XYZ
            frame.l_hand_x = (float)data[i]["L_Wrist_x"];
            frame.l_hand_y = -(float)data[i]["L_Wrist_y"];
            frame.l_hand_z = (float)data[i]["L_Wrist_z"];

            frame.r_hand_x = (float)data[i]["R_Wrist_x"];
            frame.r_hand_y = -(float)data[i]["R_Wrist_y"];
            frame.r_hand_z = (float)data[i]["R_Wrist_z"];

            animationData[i] = frame;

        }

        return animationData;
    }
}
