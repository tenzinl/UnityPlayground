using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderingManager : MonoBehaviour
{
    AnimationData[] animationData;

    public float fps = 24;

    public Transform hip;
    public Transform spine;
    public Transform neck;
    public Transform head;

    public Transform l_arm;
    public Transform r_arm;
    public Transform l_forearm;
    public Transform r_forearm;
    public Transform l_hand;
    public Transform r_hand;

    public Transform l_upleg;
    public Transform r_upleg;
    public Transform l_leg;
    public Transform r_leg;
    public Transform l_foot;
    public Transform r_foot;

    private int currentFrame = 0;
    private float startTime;

    private JointData[] joints;

    private bool _initialized = false;
    private bool _animationPlaying = false;


    public enum JointIndex: int
    {
        hip,
        spine,

        l_arm,
        l_forearm,
        l_hand,

        r_arm,
        r_forearm,
        r_hand,

        l_upleg,
        l_leg,
        l_foot,

        r_upleg,
        r_leg,
        r_foot,

        neck,
        head,
        nose,

        Count
    }
    
    private void InitializeAnimation()
    {
        startTime = Time.time;

        joints = new JointData[(int)JointIndex.Count];
        for (var i = 0; i < (int)JointIndex.Count; i++)
        {
            joints[i] = new JointData();
        }

        joints[(int)JointIndex.hip].transformComponent = hip;
        joints[(int)JointIndex.spine].transformComponent = spine;
        joints[(int)JointIndex.neck].transformComponent = neck;
        joints[(int)JointIndex.head].transformComponent = head;

        joints[(int)JointIndex.l_arm].transformComponent = l_arm;
        joints[(int)JointIndex.r_arm].transformComponent = r_arm;
        joints[(int)JointIndex.l_forearm].transformComponent = l_forearm;
        joints[(int)JointIndex.r_forearm].transformComponent = r_forearm;
        joints[(int)JointIndex.l_hand].transformComponent = l_hand;
        joints[(int)JointIndex.r_hand].transformComponent = r_hand;

        joints[(int)JointIndex.l_upleg].transformComponent = l_upleg;
        joints[(int)JointIndex.r_upleg].transformComponent = r_upleg;
        joints[(int)JointIndex.l_leg].transformComponent = l_leg;
        joints[(int)JointIndex.r_leg].transformComponent = r_leg;
        joints[(int)JointIndex.l_foot].transformComponent = l_foot;
        joints[(int)JointIndex.r_foot].transformComponent = r_foot;

        joints[(int)JointIndex.l_arm].child = joints[(int)JointIndex.l_forearm];
        joints[(int)JointIndex.l_forearm].child = joints[(int)JointIndex.l_hand];

        joints[(int)JointIndex.r_arm].child = joints[(int)JointIndex.r_forearm];
        joints[(int)JointIndex.r_forearm].child = joints[(int)JointIndex.r_hand];

        joints[(int)JointIndex.l_upleg].child = joints[(int)JointIndex.l_leg];
        joints[(int)JointIndex.l_leg].child = joints[(int)JointIndex.l_foot];

        joints[(int)JointIndex.r_upleg].child = joints[(int)JointIndex.r_leg];
        joints[(int)JointIndex.r_leg].child = joints[(int)JointIndex.r_foot];

        joints[(int)JointIndex.spine].child = joints[(int)JointIndex.neck];
        joints[(int)JointIndex.neck].child = joints[(int)JointIndex.head];
        // joints[(int)JointIndex.head].child = joints[(int)JointIndex.nose];

        foreach (JointData joint in joints)
        {
            if (joint.transformComponent != null)
                joint.init = joint.transformComponent.rotation;

            if (joint.child != null)
                joint.inverse = GetInverse(joint, joint.child);
        }
        Vector3 initForward = TriangleNormal(joints[(int)JointIndex.spine].positionData, joints[(int)JointIndex.l_upleg].positionData, joints[(int)JointIndex.r_upleg].positionData);
        joints[(int)JointIndex.hip].inverse = Quaternion.Inverse(Quaternion.LookRotation(initForward));
        joints[(int)JointIndex.hip].init = joints[(int)JointIndex.hip].transformComponent.rotation;

        Vector3 headDirection = joints[(int)JointIndex.nose].positionData - joints[(int)JointIndex.head].positionData;
        joints[(int)JointIndex.head].inverse = Quaternion.Inverse(Quaternion.LookRotation(headDirection));

        _initialized = true;
    }

    private void Start()
    {
        if (SystemInfo.deviceType == DeviceType.Desktop)
        {
            animationData = DataManager.LoadData("mistake.csv");
            _animationPlaying = true;
            InitializeAnimation();
        }
    }

    public void SetCSV(string csv)
    {
        animationData = DataManager.LoadDataString(csv);
        InitializeAnimation();
    }

    private void Update()
    {
        if (animationData == null)
            return;

        if (currentFrame >= animationData.Length - 1 || !_initialized || !_animationPlaying)
            return;

        if (Time.time - startTime >= (1 / fps))
        {
            RenderFrame(animationData[currentFrame++]);
            startTime = Time.time;
        }
    }

    private void RenderFrame(AnimationData frame)
    {
        UpdatePosition(frame);

        JointData hipJointData = joints[(int)JointIndex.hip];
        Vector3 forward = TriangleNormal(joints[(int)JointIndex.spine].positionData, joints[(int)JointIndex.l_upleg].positionData, joints[(int)JointIndex.r_upleg].positionData);
        hipJointData.transformComponent.rotation = Quaternion.LookRotation(forward) * hipJointData.inverse * hipJointData.init;

        foreach (JointData joint in joints)
        {
            if (joint.child != null)
                joint.transformComponent.rotation = Quaternion.LookRotation(joint.positionData - joint.child.positionData, forward) * joint.inverse * joint.init;
        }

        JointData headJointData = joints[(int)JointIndex.head];
        Vector3 headDirection = TriangleNormal(joints[(int)JointIndex.head].positionData, joints[(int)JointIndex.l_arm].positionData, joints[(int)JointIndex.r_arm].positionData);
        headJointData.transformComponent.rotation = Quaternion.LookRotation(headDirection) * headJointData.inverse * headJointData.init;
    }
    public void UpdatePosition(AnimationData frame)
    {
        joints[(int)JointIndex.hip].positionData = new Vector3(frame.hip_x, frame.hip_y, frame.hip_z);
        joints[(int)JointIndex.spine].positionData = new Vector3(frame.spine_x, frame.spine_y, frame.spine_z);
        joints[(int)JointIndex.neck].positionData = new Vector3(frame.neck_x, frame.neck_y, frame.neck_z);
        joints[(int)JointIndex.head].positionData = new Vector3(frame.head_x, frame.head_y, frame.head_z);
        joints[(int)JointIndex.nose].positionData = new Vector3(frame.nose_x, frame.nose_y, frame.nose_z);
        joints[(int)JointIndex.l_arm].positionData = new Vector3(frame.l_arm_x, frame.l_arm_y, frame.l_arm_z);
        joints[(int)JointIndex.r_arm].positionData = new Vector3(frame.r_arm_x, frame.r_arm_y, frame.r_arm_z);
        joints[(int)JointIndex.l_forearm].positionData = new Vector3(frame.l_forearm_x, frame.l_forearm_y, frame.l_forearm_z);
        joints[(int)JointIndex.r_forearm].positionData = new Vector3(frame.r_forearm_x, frame.r_forearm_y, frame.r_forearm_z);
        joints[(int)JointIndex.l_hand].positionData = new Vector3(frame.l_hand_x, frame.l_hand_y, frame.l_hand_z);
        joints[(int)JointIndex.r_hand].positionData = new Vector3(frame.r_hand_x, frame.r_hand_y, frame.r_hand_z);
        joints[(int)JointIndex.l_upleg].positionData = new Vector3(frame.l_upleg_x, frame.l_upleg_y, frame.l_upleg_z);
        joints[(int)JointIndex.r_upleg].positionData = new Vector3(frame.r_upleg_x, frame.r_upleg_y, frame.r_upleg_z);
        joints[(int)JointIndex.l_leg].positionData = new Vector3(frame.l_leg_x, frame.l_leg_y, frame.l_leg_z);
        joints[(int)JointIndex.r_leg].positionData = new Vector3(frame.r_leg_x, frame.r_leg_y, frame.r_leg_z);
        joints[(int)JointIndex.l_foot].positionData = new Vector3(frame.l_foot_x, frame.l_foot_y, frame.l_foot_z);
        joints[(int)JointIndex.r_foot].positionData = new Vector3(frame.r_foot_x, frame.r_foot_y, frame.r_foot_z);
    }

    private void OnDrawGizmosSelected()
    {
        if (joints != null)
        {
            Gizmos.color = Color.red;
            float width = 5f;
            DrawLine(joints[(int)JointIndex.hip].positionData, joints[(int)JointIndex.spine].positionData, width);
            DrawLine(joints[(int)JointIndex.hip].positionData, joints[(int)JointIndex.l_upleg].positionData, width);
            DrawLine(joints[(int)JointIndex.hip].positionData, joints[(int)JointIndex.r_upleg].positionData, width);
            DrawLine(joints[(int)JointIndex.l_upleg].positionData, joints[(int)JointIndex.l_leg].positionData, width);
            DrawLine(joints[(int)JointIndex.l_leg].positionData, joints[(int)JointIndex.l_foot].positionData, width);
            DrawLine(joints[(int)JointIndex.r_upleg].positionData, joints[(int)JointIndex.r_leg].positionData, width);
            DrawLine(joints[(int)JointIndex.r_leg].positionData, joints[(int)JointIndex.r_foot].positionData, width);
            DrawLine(joints[(int)JointIndex.spine].positionData, joints[(int)JointIndex.l_arm].positionData, width);
            DrawLine(joints[(int)JointIndex.spine].positionData, joints[(int)JointIndex.neck].positionData, width);
            DrawLine(joints[(int)JointIndex.spine].positionData, joints[(int)JointIndex.r_arm].positionData, width);
            DrawLine(joints[(int)JointIndex.neck].positionData, joints[(int)JointIndex.head].positionData, width);
            DrawLine(joints[(int)JointIndex.l_arm].positionData, joints[(int)JointIndex.l_forearm].positionData, width);
            DrawLine(joints[(int)JointIndex.l_forearm].positionData, joints[(int)JointIndex.l_hand].positionData, width);
            DrawLine(joints[(int)JointIndex.r_arm].positionData, joints[(int)JointIndex.r_forearm].positionData, width);
            DrawLine(joints[(int)JointIndex.r_forearm].positionData, joints[(int)JointIndex.r_hand].positionData, width);
        }
    }

    private static void DrawLine(Vector3 p1, Vector3 p2, float width)
    {
        int count = 1 + Mathf.CeilToInt(width);
        if (count == 1)
        {
            Gizmos.DrawLine(p1, p2);
        }
        else
        {
            Camera c = Camera.current;
            if (c == null)
            {
                Debug.LogError("Camera.current is null");
                return;
            }
            var scp1 = c.WorldToScreenPoint(p1);
            var scp2 = c.WorldToScreenPoint(p2);

            Vector3 v1 = (scp2 - scp1).normalized;
            Vector3 n = Vector3.Cross(v1, Vector3.forward);

            for (int i = 0; i < count; i++)
            {
                Vector3 o = 0.99f * n * width * ((float)i / (count - 1) - 0.5f);
                Vector3 origin = c.ScreenToWorldPoint(scp1 + o);
                Vector3 destiny = c.ScreenToWorldPoint(scp2 + o);
                Gizmos.DrawLine(origin, destiny);
            }
        }
    }

    private Quaternion GetInverse(JointData p1, JointData p2)
    {
        return Quaternion.Inverse(Quaternion.LookRotation(p1.transformComponent.position - p2.transformComponent.position));
    }

    private Vector3 TriangleNormal(Vector3 a, Vector3 b, Vector3 c)
    {
        Vector3 d1 = a - b;
        Vector3 d2 = a - c;

        Vector3 dd = Vector3.Cross(d1, d2);
        dd.Normalize();

        return dd;
    }


    public void SetFrameRate(float f)
    {
        fps = f;
    }

    public void StartAnimation()
    {
        _animationPlaying = true;
    }

    public void StopAnimation()
    {
        _animationPlaying = false;
    }

    public void StartStopAnimation()
    {
        _animationPlaying = !_animationPlaying;
    }

    public void GoToFrame(int frame)
    {
        currentFrame = frame;
    }

    public void SetPlayedPercentage(int percent)
    {
        currentFrame = (int)(percent / 100f * (animationData.Length - 1));
    }

    public void NextFrame()
    {
        currentFrame += 1;
        RenderFrame(animationData[currentFrame]);
    }

    public void PreviousFrame()
    {
        currentFrame -= 1;
        RenderFrame(animationData[currentFrame]);
    }
}
