﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationData
{
    // Data: Pelvis XYZ
    public float hip_x;
    public float hip_y;
    public float hip_z;

    // Data: Torso XYZ
    public float spine_x;
    public float spine_y;
    public float spine_z;

    // Data: LR UpLeg XYZ
    public float l_upleg_x;
    public float l_upleg_y;
    public float l_upleg_z;

    public float r_upleg_x;
    public float r_upleg_y;
    public float r_upleg_z;

    // Data: LR Knee XYZ
    public float l_leg_x;
    public float l_leg_y;
    public float l_leg_z;

    public float r_leg_x;
    public float r_leg_y;
    public float r_leg_z;

    // Data: LR Ankle XYZ
    public float l_foot_x;
    public float l_foot_y;
    public float l_foot_z;

    public float r_foot_x;
    public float r_foot_y;
    public float r_foot_z;

    // Data: Neck XYZ
    public float neck_x;
    public float neck_y;
    public float neck_z;

    // Data: Head XYZ
    public float head_x;
    public float head_y; 
    public float head_z;

    // Data: Nose XYZ
    public float nose_x;
    public float nose_y;
    public float nose_z;

    // Data: LR Shoulder XYZ (Could also be arm)
    public float l_arm_x;
    public float l_arm_y;
    public float l_arm_z;

    public float r_arm_x;
    public float r_arm_y;
    public float r_arm_z;

    // Data: LR Elbow XYZ
    public float l_forearm_x;
    public float l_forearm_y;
    public float l_forearm_z;

    public float r_forearm_x;
    public float r_forearm_y;
    public float r_forearm_z;

    // Data: LR Wrist XYZ
    public float l_hand_x;
    public float l_hand_y;
    public float l_hand_z;

    public float r_hand_x;
    public float r_hand_y;
    public float r_hand_z;
}

public class JointData
{
    public Vector3 positionData;

    public JointData child;

    public Quaternion init;
    public Quaternion inverse;

    public Transform transformComponent;
}
