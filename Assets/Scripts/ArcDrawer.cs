﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcDrawer : MonoBehaviour
{
    public float arcDrawRadius = 4; // Radius of circle which points on arc would belong to.

    private Vector3[] positions = new Vector3[3];
    private GameObject jointParent;
    private Transform[] joints = new Transform[3];
    private LineRenderer line;
    private Vector3[] arcPositions = new Vector3[40];
    private Vector3 centerPosition;
    private const float PI = Mathf.PI;
    // Start is called before the first frame update
    void Start() {
        line = GetComponent<LineRenderer>();
        jointParent = GameObject.Find("Joints");
        for (int i = 0; i < 3; i++) {
            joints[i] = jointParent.transform.GetChild(i); // I'm assuming this will pass by reference
            positions[i] = joints[i].position;
        }
        centerPosition = positions[1];
        //drawArc();
        drawArcAttempt2();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Given three positions (the second of which is assumed to be the middle point of the angle), return spherical coordinates for the first and third points
    // relative to the second point.
    void drawArc() {
        Vector3 relativeJointPosition1;
        Vector3 relativeJointPosition3;
        relativeJointPosition1 = positions[0] - positions[1];
        relativeJointPosition3 = positions[2] - positions[1];
        float theta1 = getTheta(relativeJointPosition1);
        float phi1 = getPhi(relativeJointPosition1);
        float theta2 = getTheta(relativeJointPosition3);
        float phi2 = getPhi(relativeJointPosition3);

        // Debugging system lines
        float magnitude = relativeJointPosition1.magnitude;
        float magnitudeXY = magnitude * Mathf.Sin(phi1);
        Vector3 debugPosition;
        debugPosition.x = magnitudeXY * Mathf.Cos(theta1);
        debugPosition.y = magnitudeXY * Mathf.Sin(theta1);
        debugPosition.z = magnitude * Mathf.Cos(phi1);
        print("Theta1 is: " + theta1 + " magnitude is: " + magnitude);
        printVector(relativeJointPosition1);
        printVector(debugPosition);
        // So z is good, but x and y are f'ed up. (Also I need to resolve quadrant handling for all 4.)
        // I think I'm using wrong magnitude for x and y calculations (I need the magnitude purely in the x-y plane,
        // which should easily be doable by multiplying overall magnitude by sin(phi). EYY that worked.
        // Now we just need to do some -/+ checking to determine when to flip signs of x, y, or z.

        // Mathf functions are all in RADIANS, whereas actualAngle below is in degrees.
        double actualAngleDegrees = Vector3.Angle(relativeJointPosition1, relativeJointPosition3); // Returns minimum angle between two legs
        double actualAngleRadians = Mathf.Deg2Rad * actualAngleDegrees;
        int numberOfPoints = ((int)actualAngleDegrees / 5); // Add one point for every 5 degrees. Also, apparently there is no long long in c# lol.

        float thetaBegin, thetaEnd, phiBegin, phiEnd;
        if (theta1 <= theta2) {
            thetaBegin = theta1;
            phiBegin = phi1;
            thetaEnd = theta2;
            phiEnd = phi2;
        } else {
            thetaBegin = theta2;
            phiBegin = phi2;
            thetaEnd = theta1;
            phiEnd = phi1;
        }

        // Check for shortest theta to go through (Fixes disjointed issue)
        if (Mathf.Abs(thetaEnd - thetaBegin) > Mathf.Abs(thetaEnd - (thetaBegin + 2 * Mathf.PI))) {
            thetaBegin += 2 * Mathf.PI;
        }

        double deltaTheta = (thetaEnd - thetaBegin) / numberOfPoints;
        double deltaPhi = (phiEnd - phiBegin) / numberOfPoints;

        double curTheta = thetaBegin;
        double curPhi = phiBegin;
        float arcDrawRadiusXY = arcDrawRadius * Mathf.Sin(phiBegin);
        Vector3 curRelativePosition;
        for (int i = 0; i < numberOfPoints; i++) {
            curTheta += deltaTheta;
            curPhi += deltaPhi;
            arcDrawRadiusXY = arcDrawRadius * Mathf.Sin((float)curPhi);
            curRelativePosition.x = arcDrawRadiusXY * Mathf.Cos((float)curTheta);
            curRelativePosition.y = arcDrawRadiusXY * Mathf.Sin((float)curTheta);
            curRelativePosition.z = arcDrawRadius * Mathf.Cos((float)curPhi);
            arcPositions[i] = curRelativePosition + centerPosition;
        }
        line.positionCount = numberOfPoints;
        line.SetPositions(arcPositions);
    }

    void printVector(Vector3 v) {
        print("x: " + v.x + " y: " + v.y + " z: " + v.z);
    }

    float getTheta(Vector3 vector) {
        // Don't worry about divide by 0 etc. as IEEE 754 provisions for floats to represent NaN and INF
        if (Mathf.Abs(vector.x) <= 0.001 && Mathf.Abs(vector.y) <= 0.001) {
            return PI / 2;
        }
        float theta = Mathf.Atan(vector.y / vector.x);
        if (theta > 0 && vector.y < 0) {
            theta += Mathf.PI;
        }
        if (theta < 0) {
            theta += Mathf.PI;
            if (vector.y < 0) {
                theta += Mathf.PI;
            }
        }
        return theta;
    }

    float getPhi(Vector3 vector) {
        float phi = Mathf.Acos(vector.z / vector.magnitude);
        return phi;
    }

    // Returns the Cartesian equivalent of a given spherical coordinate as a Vector3
    Vector3 sphericalToCartesian(float radius, float theta, float phi) {
        Vector3 ans;
        ans.z = radius * Mathf.Cos(phi);
        float xyMagnitude = radius * Mathf.Sin(phi);
        ans.x = xyMagnitude * Mathf.Cos(theta);
        ans.y = xyMagnitude * Mathf.Sin(theta);
        return ans;
    }

    // Returns a coordinates equivalent coordinate in a reference frame that is rotated by theta and phi respectively.
    // theta is measured from x axis towards y, and should be measured to the x-axis of the rotated frame (which is always in xy plane)
    // phi is measured from z axis to z-axis of rotated frame (z is vertical). Measures rotation around x-axis.
    Vector3 rotateCoordinates(Vector3 v, float theta, float phi) {
        // Equations I have need angle from rotated coord to original, not from original to rotated
        // so the below two lines fix them properly. weird - PI/2 etc. are because theta starts at PI/2 in the normal axes.
        theta = PI-theta;
        phi = -phi;
        Vector3 ans;
        ans.x = v.x * Mathf.Cos(theta - PI / 2) + v.y * Mathf.Sin(phi + PI / 2) * Mathf.Cos(theta) + v.z * Mathf.Sin(phi) * Mathf.Cos(theta);
        ans.y = v.x * Mathf.Sin(theta - PI / 2) + v.y * Mathf.Sin(phi + PI / 2) * Mathf.Sin(theta) + v.z * Mathf.Sin(phi) * Mathf.Sin(theta);
        ans.z = v.y * Mathf.Cos(phi + PI / 2) + v.z * Mathf.Cos(phi);
        return ans;
    }

    // Given rotated coordinates and the theta and phi originally used for conversion, returns the original coordinates.
    Vector3 reverseRotation(Vector3 v, float theta, float phi) {
        // Idea should be to reverse phi rotation first so that x-y plane matches original (and theta conversion will then work).
        return new Vector3(0,0,0);
    }

    // Invalid AABB (axially aligned bounding box), my guess is that that means line positions have infinite values somewhere, causing
    // line renderer object to have an impossible transform and bounding box.
    void drawArcAttempt2() {
        Vector3[] relativePositions = new Vector3[3];
        for (int i = 0; i < 3; i++) {
            relativePositions[i] = positions[i] - positions[1];
        }
        float rotateTheta = getTheta(relativePositions[0]);
        float rotatePhi = getPhi(relativePositions[0]);

        print("RotateTheta and RotatePhi: " + rotateTheta + " " + rotatePhi);
        Vector3[] rotatedPositions = new Vector3[3];
        for (int i = 0; i < 3; i++) {
            rotatedPositions[i] = rotateCoordinates(relativePositions[i], rotateTheta, rotatePhi);
            print("rotatedPosition #" + i + ": " + rotatedPositions[i]); // rotatedPosition 0 should be (0,0, |relativePositions[0]|);
        }
        print("magnitude of relativePositions[0]: " + relativePositions[0].magnitude);

        float[] rotatedThetas = new float[3];
        float[] rotatedPhis = new float[3];
        for (int i = 0; i < 3; i++) {
            rotatedThetas[i] = getTheta(rotatedPositions[i]);
            rotatedPhis[i] = getPhi(rotatedPositions[i]);
        }
        print("here1");
        float actualAngle = Vector3.Angle(relativePositions[0], relativePositions[2]);
        int numberOfPoints = (int)actualAngle / 5;
        Vector3[] linePositions = new Vector3[numberOfPoints + 1];
        linePositions[0] = relativePositions[0] + positions[1]; // We actually have arc positions, global V3 array
        float deltaRotatedTheta = (rotatedThetas[2] - rotatedThetas[0]) / numberOfPoints; // should be unnecessary if we just set first point
        float deltaRotatedPhi = (rotatedPhis[0] - rotatedPhis[1]);
        float curRotatedTheta = rotatedThetas[0], curRotatedPhi = rotatedPhis[0];
        print("here2");
        for (int i = 1; i <= numberOfPoints; i++) {
            curRotatedTheta += deltaRotatedTheta;
            curRotatedPhi += deltaRotatedPhi;
            Vector3 tempRotatedCoordinates = sphericalToCartesian(arcDrawRadius, curRotatedTheta, curRotatedPhi);
            linePositions[i] = rotateCoordinates(tempRotatedCoordinates, -rotateTheta, -rotatePhi); // The reversion might need reworking
            print("here3");
        }

        line.positionCount = numberOfPoints + 1;
        line.SetPositions(linePositions);
        return;
    }
}