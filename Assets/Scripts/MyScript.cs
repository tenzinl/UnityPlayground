﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class MyScript : MonoBehaviour
{
    protected LineRenderer line;
    protected Vector3 Direction;
    protected Vector3 position;
    // Start is called before the first frame update
    void Start()
    {
        line = GetComponent<LineRenderer>();
        Direction.y = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Direction.x = (Input.GetAxisRaw("Horizontal"));
        Direction.z = (Input.GetAxisRaw("Vertical"));

        position += Direction * 50 * Time.deltaTime;

        line.SetPosition(1, position);
    }
}
