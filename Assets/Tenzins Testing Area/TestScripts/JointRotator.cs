﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Joint Rotator rotates whatever game object it is attached to at a constant rate
// defined by a Serialized field which can be edited in the unity editor.

public class JointRotator : MonoBehaviour
{
    // Serialize these fields to allow them to be set in the Unity editor.
    [SerializeField] private Transform targetJoint;
    [SerializeField] private float rotationSpeedX;
    [SerializeField] private float rotationSpeedY;
    [SerializeField] private float rotationSpeedZ;
    [SerializeField] private float RotationFactor = 0.33f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Fixed update is called at a constant rate with respect to time (about 50 Hz)
    private void FixedUpdate() 
    {
        Vector3 rotationVector = new Vector3(rotationSpeedX, rotationSpeedY, rotationSpeedZ);
        targetJoint.Rotate(rotationVector * RotationFactor, Space.Self);
    }
}

// For details about the Unity Rotation API see here: https://docs.unity3d.com/ScriptReference/Transform.Rotate.html;